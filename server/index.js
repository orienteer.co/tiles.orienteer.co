const app = require("express")();

const { Pool } = require("pg");

const CREATE_BBOX = `
create or replace function BBox (z int, x int, y int, srid int = 3857)
    returns geometry
    language plpgsql immutable as
$func$
declare
    max numeric := 20037508.34;
    res numeric := (max*2)/(2^z);
    bbox geometry;
begin
    bbox := ST_MakeEnvelope(
        -max + (x * res),
        max - (y * res),
        -max + (x * res) + res,
        max - (y * res) - res,
        3857
    );
    if srid = 3857 then
        return bbox;
    else
        return ST_Transform(bbox, srid);
    end if;
end;
$func$;
`;
app.set(
  "tile-pool",
  new Pool({
    max: 10,
    host: process.env.PG_HOST,
    user: process.env.PG_USER,
    database: process.env.PG_DATABASE,
    port: process.env.PG_PORT || 5432,
    password: process.env.PG_PASSWORD,
  })
);

app
  .get("tile-pool")
  .connect()
  .then((c) => {
    c.query(CREATE_BBOX);
    app.set("tile-admin-connection", c);
  });

app.on("close", () => {
  app.get("tile-pool").end();
});

const currentTolerance = 1;

app.get("/ownership/:z/:x/:y.mvt", async (req, res) => {
  res.set('Access-Control-Allow-Origin',req.get('origin'));
          
  const { z, x, y } = req.params;
  if (z < 10)
    return res.status(404).send({ error: "Too low zoom - try 10 or higher" });
  // calculate the bounding polygon for this tile

  // Query the database, using ST_AsMVTGeom() to clip the geometries
  // Wrap the whole query with ST_AsMVT(), which will create a protocol buffer
  const SQL = `
  SELECT 'surfacemanagementagency' as name, ST_AsMVT(q, 'surfacemanagementagency', 4096, 'geom') as tile
  FROM (
    SELECT admin_unit_name, admin_unit_type , admin_dept_code, admin_agency_code, hold_dept_code, hold_agency_code  ,
      ST_Simplify(ST_AsMvtGeom(
        shape,
        BBox(${z},${x}, ${y}),
        4096,
        256,
        true
      ), ${currentTolerance}) AS geom
    FROM public.surfacemanagementagency
    WHERE shape && BBox(${z},${x}, ${y})
    AND ST_Intersects(shape, BBox(${z},${x}, ${y}))
    ) as q
  `;

  let cancelled = false;
  let db;

  try {
    const cancel = async () => {
      cancelled = true;
      let cancelConnection = null;

      try {
        if (db) {
          cancelConnection = app.get("tile-admin-connection");
          cancelConnection.cancel(db);
        }
      } catch (x) {
        console.error("CANCEL ERROR: ", x);
      }
    };

    req.on("aborted", cancel);
    req.on("cancel", cancel);

    db = await app.get("tile-pool").connect();
    if (cancelled) {
      throw new Error("cancelled");
    }

    const { rows, error } = await db.query(SQL);

    if (error) throw error;

    const layer = rows[0];

    // set the response header content type
    res.set("Content-Type", "application/x-protobuf");

    // trigger catch if the vector tile has no data, (return a 204)
    if (layer.tile.length === 0) {
      res.status(204);
    }

    // send the tile!
    res.send(layer.tile);
  } catch (e) {
    res.status(500).send({
      error: e.toString(),
    });
  } finally {
    if (db) db.release();
  }
});

module.exports = app;
