const { Ingress } = require('@brd.com/deploy-it');

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || 'debug';
const HOST = process.env.APP_HOST || 'test-host.co';
const PATH = process.env.APP_PATH || '/test-path';

module.exports = () => {
  const I  = new Ingress(ENV_SLUG);

  I.ingressClass('nginx');

  if(process.env.CI_JOB_STAGE != 'deploy-review') {
    I.set('metadata.annotations',{
      'cert-manager.io/acme-challenge-type': 'dns01',
      'cert-manager.io/cluster-issuer': 'letsencrypt-production',
    });
  }

  const host = I.host(`${HOST}`)
        .path(`${PATH}`,ENV_SLUG,8080);

  host.tls(`tls-${process.env.CI_JOB_STAGE}`);

  return I;
};
