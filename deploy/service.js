const { Service } = require('@brd.com/deploy-it');

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || 'debug-slug';

module.exports = () => {
  const S = new Service(ENV_SLUG);

  S.port('app',8080)
    .selector('app',ENV_SLUG);

  return S;
}
