const {
  Deployment,
  Service,
  PersistentVolumeClaim
} = require("@brd.com/deploy-it");

module.exports = function() {
  const D = new Deployment(`postgis`);
  D.template
    .container("postgis")
    .port("postgres", 5432)
    .image("mdillon/postgis:11-alpine")
    // Postgres:
    .secretEnv("POSTGRES_USER", { name: `postgres-root`, key: "user" })
    .secretEnv("POSTGRES_PASSWORD", {
      name: `postgres-root`,
      key: "password"
    })
    .secretEnv("POSTGRES_DB", {
      name: `postgres-root`,
      key: "database"
    })
    .env("PGDATA", "/db-data/data")
    .volumeMount("postgis-storage", "/db-data");

  D.template.volume("postgis-storage", {
    persistentVolumeClaim: { claimName: `postgis` }
  });

  const S = new Service(`postgis`);
  S.port("postgres", 5432).selector("app", `postgis`);

  const PVC = new PersistentVolumeClaim(`postgis`);
  PVC.size("5Gi");
  PVC.accessMode("ReadWriteOnce");

  return [D, PVC, S];
};
