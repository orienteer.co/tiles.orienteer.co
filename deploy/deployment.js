const { Deployment } = require('@brd.com/deploy-it');

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || 'debug';
const IMAGE = process.env.IMAGE || 'debug.docker/image:tag';

module.exports = () => {
  const D = new Deployment(ENV_SLUG);

  D.replicas(process.env.REPLICAS || 1)

  // is this actually better?
  
  D.template.container('app')
    .port('app',8080)
    .env('PORT',8080)
    .env('NODE_ENV','production')
    .env('PG_HOST','postgis')
    
    .secretEnv('PG_USER',{ name: 'postgres-root', key: 'user' })
    .secretEnv('PG_PASSWORD',{ name: 'postgres-root', key: 'password' })
    .secretEnv('PG_DATABASE',{ name: 'postgres-root', key: 'database' })

    .secretEnv('DATABASE_URL',{ name: 'postgres', key: 'database-url' })
    .image(IMAGE);
  
  return D;
}
