image: registry.gitlab.com/brd.com/deploy-it/ci-base:1.0
services:
  - docker:dind

# The build stuff:

stages:
  - lint
  - build
  - deploy-review
  - test
  - deploy-stage
  - test-stage
  - deploy-prod
  - stop-review
  - stop-deploy
  
variables:
  DEPLOYER_IMAGE: registry.gitlab.com/brd.com/deploy-it/deployer:1.0
  REPLICAS: 1
  NODE_ENV: production
  KUBE_NAMESPACE: tile-server
  PRODUCTION_URL: https://tiles.orienteer.co/
  GOOGLE_APPLICATION_CREDENTIALS: /tmp/google-app-credentials.json
  GIT_SUBMODULE_STRATEGY: normal
  SENTRY_ORG: breadwallet-llc
  # SENTRY_PROJECT: tile-server

# Use the global script to change things about every script.  Setting
.global: &global
  stage: gets-overwritten # <-- this line is to make this not empty
  

# lint:
# <<: *global
#   stage: lint
#   allow_failure: true
#   script:
#     - env
#     - pwd
#     - builder npm run lint


test-hotfix:
  <<: *global
  stage: test
  allow_failure: true
  only:
    - /^hotfix\//
  script:
    - echo "Skipping tests - we don't have any yet" ; exit 0
    - builder npm run ci-test


test-stage:
  <<: *global
  stage: test-stage
  only:
    - /^release\//
    - /^hotfix\//
  script:
    - echo "Skipping tests - we don't have any yet" ; exit 0
    - builder npm run ci-test-stage

# test-hotfix:
# <<: *global
#   stage: test
#   only:
#     - /^hotfix\//
#   allow_failure: true
#   script:
#     npm run test

build:
  <<: *global
  stage: build
  only:
    - branches
    - /^(.*-)?dev-.*$/
    - /^staging-.*$/
  artifacts:
    paths:
      - .nuxt
      - bundle-report.html
    name: "$CI_BUILD_REF"
  script:
    - if docker pull $ROOT_IMAGE ; then echo "Already built." ; exit 0 ; fi
    # - builder npm run build
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest  --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest --tag $ROOT_IMAGE -f docker/Dockerfile-prod .
    - docker push $ROOT_IMAGE
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-latest


# test:
#   <<: *global
#   stage: test
#   # when: manual
#   allow_failure: true
#   only:
#     - branches
#   except:
#     - production
#   artifacts:
#     paths:
#       - cypress/screenshots
#       - cypress/videos
#     when: always
#   script:
#     - cypress_test $ROOT_IMAGE

deploy-prod:
  <<: *global
  image: $DEPLOYER_IMAGE
  stage: deploy-prod
  environment:
    name: production
    url: $PRODUCTION_URL
  only:
    refs:
      - master
  script:
    - auth
    - export REPLICAS=${PRODUCTION_REPLICAS:=2}
    - create_gitlab_registry_secret
    # Maybe we'll need to migrate the DB in the future?
    # - kubectl run init-db-${CI_JOB_ID} -n $KUBE_NAMESPACE --rm --generator=run-pod/v1  --attach=true --image=$ROOT_IMAGE --overrides="$(db/tools/init-pod-tmpl.js bash -c 'bin/setup.js init-review-db && bin/worker --once && bin/migrate')"
    - deploy

deploy-staging:
  <<: *global
  image: $DEPLOYER_IMAGE
  stage: deploy-stage
  environment:
    name: staging
    url: $STAGING_URL
  only:
    variables:
      - $NO_GO
    refs:
      - master
  script:
    - auth
    - setup_secrets
    - export REPLICAS=${STAGING_REPLICAS:=1}
    - create_gitlab_registry_secret
    - kubectl run init-db-${CI_JOB_ID} -n $KUBE_NAMESPACE --rm --generator=run-pod/v1  --attach=true --image=$ROOT_IMAGE --overrides="$(db/tools/init-pod-tmpl.js bash -c 'bin/setup.js init-review-db && bin/worker --once && bin/migrate')"
    - deploy

stop-deploy:
  <<: *global
  stage: stop-deploy
  only:
    refs:
      - /^staging-.*$/
  script:
    - echo this is not meant to be merged anywhere, this is dead-end staging testing code
    - exit 1

deploy-review:
  <<: *global
  stage: deploy-review
  image: $DEPLOYER_IMAGE
  environment:
    name: tile-server$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.tile-server.orienteer.co/
    on_stop: stop-review
  only:
    refs:
      - branches
    variables:
      - $GOOGLE_APPLICATION_CREDENTIALS_DATA
      - $GOOGLE_APPLICATION_CREDENTIALS_REVIEW_DATA
  except:
    - production
    - /^hotfix\//
    - /^release\//
    - /^staging-.*/
  variables:
    DEPLOY_TMPL_FILE: deploy/review.yaml
  script:
    - echo ${GOOGLE_APPLICATION_CREDENTIALS_REVIEW_DATA:=$GOOGLE_APPLICATION_CREDENTIALS_DATA} > ${GOOGLE_APPLICATION_CREDENTIALS} && sleep 1
    - export GCLOUD_PROJECT_ID=${GCLOUD_REVIEW_PROJECT_ID:=$GCLOUD_PROJECT_ID}
    - export KUBE_NAMESPACE=${KUBE_REVIEW_NAMESPACE:=$KUBE_NAMESPACE}
    - export KUBE_CLUSTER=${KUBE_REVIEW_CLUSTER:=$KUBE_CLUSTER}
    - google_auth
    - deploy

stop-review:
  <<: *global
  stage: stop-review
  image: $DEPLOYER_IMAGE
  when: manual
  environment:
    name: tile-server$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.tile-server.orienteer.co/
    action: stop
  only:
    refs:
      - branches
    variables:
      - $GOOGLE_APPLICATION_CREDENTIALS_DATA
      - $GOOGLE_APPLICATION_CREDENTIALS_REVIEW_DATA
  except:
    - production
    - /^hotfix\//
    - /^release\//
    - /^staging-.*/
  script:
    - echo ${GOOGLE_APPLICATION_CREDENTIALS_REVIEW_DATA:=$GOOGLE_APPLICATION_CREDENTIALS_DATA} > ${GOOGLE_APPLICATION_CREDENTIALS} && sleep 1
    - export GCLOUD_PROJECT_ID=${GCLOUD_REVIEW_PROJECT_ID:=$GCLOUD_PROJECT_ID}
    - export KUBE_NAMESPACE=${KUBE_REVIEW_NAMESPACE:=$KUBE_NAMESPACE}
    - export KUBE_CLUSTER=${KUBE_REVIEW_CLUSTER:=$KUBE_CLUSTER}
    - google_auth
    - stop_review

.functions: &functions |
  function docker-env() {
    DEFAULT="-v -E "'^(PATH|HOSTNAME|TERM|HOME|PWD|alias|DOCKER_|BASH*|SHELL*|\s)'
    PATTERN=${@:-$DEFAULT}
    compgen -v | grep $PATTERN | awk '{ print "-e " $1 }'
  }

  function prepare_builder() {
    if [ -z "$DI_BUILDER_DOCKERFILE" ]; then
        if [ -f docker/Dockerfile-dev ]; then
            export DI_BUILDER_DOCKERFILE=docker/Dockerfile-dev
        elif [ -f Dockerfile ]; then
            export $BUILDER_DOCKERFILE=Dockerfile
        else
            echo "No dockerfile for builder found - place at docker/Dockerfile-dev or Dockerfile, or set variable DI_BUILDER_DOCKERFILE to where it is"
            exit 1
        fi
    fi

    if ! docker pull $BUILDER_IMAGE ; then
      docker build -t $BUILDER_IMAGE -f $DI_BUILDER_DOCKERFILE .
      docker push $BUILDER_IMAGE
    fi
  }

  function random() {
    n=${1:-16}
    xxd -l "$n" -p /dev/urandom | tr -d " \n"
  }

  function prep_host_pwd() {
    # Find the mounting point for PWD in the owner host - this means we can do DIND using the socket thing *AND* build outside, *AND* it should work on shared runners:
      CONTAINER=$(docker ps | grep $HOSTNAME | awk '{ print $1 }' || echo )
      OUTER_PWD=$(docker inspect $CONTAINER | grep $HOSTNAME | grep $(dirname $PWD) | grep -o -e '\".*:' | tr '"' ' ' | tr ':' ' ' | awk '{$1=$1};1' || echo)
      if [ ! "$OUTER_PWD" ]; then
        export HOST_PWD=$PWD
      else
        export HOST_PWD="$OUTER_PWD""/"$(basename $PWD)
      fi
    }


  function builder() {
      prepare_builder

      BUILDER_PWD=$(docker run --rm --entrypoint "" $BUILDER_IMAGE pwd)

      debug docker run --rm -u=$UID:$(id -g $USER) \
             $(docker-env) \
             -v "$HOST_PWD":"$BUILDER_PWD" \
             -v $BUILDER_PWD/node_modules \
             -v "$HOST_PWD"/node_modules/.cache:"$BUILDER_PWD"/node_modules/.cache \
             $BUILDER_IMAGE "$@"

      docker run --rm -u=$UID:$(id -g $USER) \
             $(docker-env) \
             -v "$HOST_PWD":"$BUILDER_PWD" \
             -v $BUILDER_PWD/node_modules \
             -v "$HOST_PWD"/node_modules/.cache:"$BUILDER_PWD"/node_modules/.cache \
             $BUILDER_IMAGE "$@"
  }


  function setup_secrets() {
    if ! kubectl -n $KUBE_NAMESPACE get secret ${CI_ENVIRONMENT_SLUG}-keys ; then
      echo Creating keys secret ${CI_ENVIRONMENT_SLUG}-keys
      echo Random $(random 32)

      db_auth_user="${CI_ENVIRONMENT_SLUG}-auth"
      db_auth_password=p$(random 32)
      db_owner_user="${CI_ENVIRONMENT_SLUG}-owner"
      db_owner_password=p$(random 32)

      kubectl -n $KUBE_NAMESPACE create secret generic ${CI_ENVIRONMENT_SLUG}-keys \
        --from-literal=cookie-secret=$(random 32) \
        --from-literal=jwt-secret=$(random 32) \
        --from-literal=db-name=${CI_ENVIRONMENT_SLUG} \
        --from-literal=db-auth-user=${db_auth_user} \
        --from-literal=db-auth-password=${db_auth_password} \
        --from-literal=db-owner-user=${db_owner_user} \
        --from-literal=db-owner-password=${db_owner_password}
    fi


  }

  function create_gitlab_registry_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi

    kubectl create secret -n "$KUBE_NAMESPACE" \
      docker-registry gitlab-registry \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
  }

  function auth() {
    mkdir -p ~/.kube
    curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/kubernetes/clusters/$DO_KUBE_CLUSTER/kubeconfig" > /tmp/config
    export KUBECONFIG=/tmp/config

    # echo stall out forever
    # tail -f /dev/null || echo Done forever

  }


  function semver() {
    if [[ $1 == *"-"* ]]
    then
      version=${1%%-*}
      rest=${1#*-}
    else
      version=$1
      rest=
    fi

    p1=$(awk -F . '{ print $1 }' <<< $version)
    p2=$(awk -F . '{ print $2 }' <<< $version)
    p3=$(awk -F . '{ print $3 }' <<< $version)

    version=${p1:-0}.${p2:-0}.${p3:-0}

    if [ -z $rest ]
    then
      echo $version
    else
      echo $version-$rest
    fi
  }

  function get-version() {
    if [ -z $CI_BUILD_TAG ]
    then
      if ! recent_tag=$(git describe --tags --abbrev=0 2> /dev/null) ; then
        echo ${CI_COMMIT_SHA:0:8}
        return
      fi

      commits_since=$(echo $(git rev-list ${recent_tag}..HEAD | wc -l))

      # check to see if this is a "beta" tag (i.e. 1.0-beta)
      if [[ $recent_tag =~ ([0-9\.]*)-([a-zA-Z]*) ]] ; then
        main_v=${BASH_REMATCH[1]}
        prerelease=${BASH_REMATCH[2]}

        echo $main_v-$prerelease.$commits_since
      else
        # is the tag a full semver?
        if [[ $recent_tag =~ ^([\.0-9]+)\.([0-9]+)\.([0-9\.]+) ]]; then
          # It's a fully-specified version, tag on a build identifier afterwards:
          echo $BASH_REMATCH-b${commits_since}
        elif [[ $recent_tag =~ ([0-9]+)\.([0-9]+) ]]; then
          # It's only got two numbers, tack on a third for number of commits since:
          echo $BASH_REMATCH.$commits_since
        else
          # it's an under-specified version, just throw out trying to make it a semver:
          git describe --tags
        fi
      fi

      # _log "No build tag :("
      # export v=$(git describe --tags --abbrev=1 | awk -F '-' '{print $1"."$2}' | sed 's|\.$||' | sed 's|\.\.||' )
      # export VERSION=$(coerceSemver $v)
    else
      echo $CI_BUILD_TAG
    fi
  }

  function debug() {
    echo "$@"
    "$@"
  }

before_script:
  - export BUILDER_IMAGE=$CI_REGISTRY_IMAGE/builder:$(sha1sum docker/Dockerfile-dev package.* | sha1sum | awk '{print $1}')
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com || echo 'failed to login to docker'
  - *functions
  - get-version
  - export VERSION=$(get-version)
  - export IMAGE=$CI_REGISTRY_IMAGE:$VERSION
  - export ROOT_IMAGE=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  - echo IMAGE $IMAGE
  - echo IMAGE $ROOT_IMAGE

