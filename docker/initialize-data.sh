#!/bin/bash

file=BLM_National_Surface_Management_Agency
if [ ! -f ${file}.zip ]; then
    curl https://gis.blm.gov/EGISDownload/LayerPackages/${file}.zip -o ${file}.zip
fi

if [ ! -d ${file} ]; then
    unzip ${file}.zip
fi

ogr2ogr -progress -overwrite -f "PostgresSQL" PG:"host=${PG_HOST} port=${PG_PORT} user=${PG_USER} password=${PG_PASSWORD} dbname=${PG_DATABASE}" BLM_National_Surface_Management_Agency/sma_wm.gdb -nlt MULTIPOLYGON	

