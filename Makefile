up: data/BLM_National_Surface_Management_Agency
	docker network create orienteer-tile-server || echo "netowrk exists"
	docker-compose up -d
	docker build -t initialize-gdal -f docker/Dockerfile-gdal docker/
	docker run --rm -v $(PWD):$(PWD) -w $(PWD)/data --network=orienteer-tile-server -e PG_HOST=postgis -e PG_PORT=5432 \
		-e PG_USER=user -e PG_PASSWORD=pass -e PG_DATABASE=land-ownership initialize-gdal


